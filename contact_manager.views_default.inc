<?php
/**
 * @file
 * Contains default views on behalf of the cntact_manager module.
 */

/**
 * Implementation of hook_default_view_views().
 */
function contact_manager_views_default_views() {
$view = new view;
$view->name = 'Contact_Manager_Contact_List';
$view->description = 'List of Users Contacts';
$view->tag = 'Contact List';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('relationships', array(
  'field_profile_id_nid' => array(
    'label' => 'User Profile',
    'required' => 0,
    'delta' => -1,
    'id' => 'field_profile_id_nid',
    'table' => 'node_data_field_profile_id',
    'field' => 'field_profile_id_nid',
    'relationship' => 'none',
  ),
));
$handler->override_option('fields', array(
  'field_first_name_value' => array(
    'label' => 'First Name',
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'default',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_first_name_value',
    'table' => 'node_data_field_first_name',
    'field' => 'field_first_name_value',
    'relationship' => 'none',
  ),
  'field_last_name_value' => array(
    'label' => 'Last Name',
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'default',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_last_name_value',
    'table' => 'node_data_field_last_name',
    'field' => 'field_last_name_value',
    'relationship' => 'none',
  ),
  'field_profile_id_nid' => array(
    'label' => 'User Profile',
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'default',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_profile_id_nid',
    'table' => 'node_data_field_profile_id',
    'field' => 'field_profile_id_nid',
    'relationship' => 'none',
  ),
  'field_email_value' => array(
    'label' => 'Email',
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'plain',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_email_value',
    'table' => 'node_data_field_email',
    'field' => 'field_email_value',
    'relationship' => 'none',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'field_phone_value' => array(
    'label' => 'Phone',
    'link_to_node' => 0,
    'label_type' => 'widget',
    'format' => 'default',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_phone_value',
    'table' => 'node_data_field_phone',
    'field' => 'field_phone_value',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'edit_node' => array(
    'label' => 'Edit link',
    'text' => 'Edit Contact',
    'exclude' => 0,
    'id' => 'edit_node',
    'table' => 'node',
    'field' => 'edit_node',
    'relationship' => 'none',
  ),
));
$handler->override_option('sorts', array(
  'field_email_value' => array(
    'order' => 'ASC',
    'id' => 'field_email_value',
    'table' => 'node_data_field_email',
    'field' => 'field_email_value',
    'relationship' => 'none',
  ),
  'field_first_name_value' => array(
    'order' => 'ASC',
    'id' => 'field_first_name_value',
    'table' => 'node_data_field_first_name',
    'field' => 'field_first_name_value',
    'relationship' => 'none',
  ),
  'field_last_name_value' => array(
    'order' => 'ASC',
    'id' => 'field_last_name_value',
    'table' => 'node_data_field_last_name',
    'field' => 'field_last_name_value',
    'relationship' => 'none',
  ),
));
$handler->override_option('filters', array(
  'type' => array(
    'operator' => 'in',
    'value' => array(
      'contact_manager' => 'contact_manager',
    ),
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'type',
    'table' => 'node',
    'field' => 'type',
    'relationship' => 'none',
  ),
  'uid_current' => array(
    'operator' => '=',
    'value' => 1,
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'uid_current',
    'table' => 'users',
    'field' => 'uid_current',
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'perm',
  'perm' => 'access contact manager',
));
$handler->override_option('title', 'Contact List');
$handler->override_option('items_per_page', 20);
$handler->override_option('use_pager', 'mini');
$handler->override_option('distinct', 0);
$handler->override_option('style_plugin', 'table');
$handler->override_option('style_options', array(
  'grouping' => '',
  'override' => 1,
  'sticky' => 0,
  'order' => 'desc',
  'columns' => array(
    'field_first_name_value' => 'field_first_name_value',
    'field_last_name_value' => 'field_last_name_value',
    'field_profile_id_nid' => 'field_profile_id_nid',
    'field_email_value' => 'field_email_value',
    'field_phone_value' => 'field_phone_value',
    'edit_node' => 'edit_node',
  ),
  'info' => array(
    'field_first_name_value' => array(
      'sortable' => 1,
      'separator' => '',
    ),
    'field_last_name_value' => array(
      'sortable' => 1,
      'separator' => '',
    ),
    'field_profile_id_nid' => array(
      'separator' => '',
    ),
    'field_email_value' => array(
      'sortable' => 1,
      'separator' => '',
    ),
    'field_phone_value' => array(
      'sortable' => 1,
      'separator' => '',
    ),
    'edit_node' => array(
      'separator' => '',
    ),
  ),
  'default' => 'field_email_value',
));
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->override_option('path', 'contact_manager/list');
$handler->override_option('menu', array(
  'type' => 'normal',
  'title' => 'Contact List',
  'weight' => '0',
  'name' => 'navigation',
));
$handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'weight' => 0,
));

$views[$view->name] = $view;

return $views;
}
