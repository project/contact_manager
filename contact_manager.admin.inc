<?php
// $Id

/**
 * Contact Manager Module administration settings
 */
function contact_manager_admin_settings_form() {
  $pfields = contact_manager_content_type_fields('profile');
  $cmfields = contact_manager_content_type_fields('contact_manager');
  $form = array();
  $mappings = variable_get('contact_manager_field_mappings', array());
  if (is_array($mappings) && !empty($mappings)) {
    $rows = array();
    $header = array('contact_manager' => t('Contact Field'), 'profile' => t('Profile Field'), 'link' => t('Operation'));
    foreach ($mappings as $k => $v) {
      $rows[] = array('contact_manager' => $cmfields[$k], 'profile' => $pfields[$v], 'link' => l(t('Delete'), 'admin/settings/contact_manager/mappings/delete/'. $k));
      unset($cmfields[$k]);
    }
    // Only display this section if the mappings are not empty
    if (!empty($rows)) {
      $form['mappings'] = array('#value' => theme('table', $header, $rows));
    }
  }

  $form['contact_manager'] = array(
    '#type' => 'select',
    '#title' => t('Contact Field'),
    '#options' => $cmfields,
    '#description' => t('Contact Manager field to create a mapping for'),
  );
  $form['profile'] = array(
    '#type' => 'select',
    '#title' => t('Profile Field'),
    '#options' => $pfields,
    '#description' => t('Content Profile field to create a mapping to'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['#theme'] = 'contact_manager_mappings_form';
  return $form;
}

/**
 * Submit handler for the form
 */
function contact_manager_admin_settings_form_submit(&$form, &$form_state) {
  $mappings = variable_get('contact_manager_field_mappings', array());
  $contact_manager = $form_state['values']['contact_manager'];
  $profile = $form_state['values']['profile'];
  if (isset($mappings[$contact_manager])) {
    drupal_set_message(t('User Profile Field has already been mapped, you must remove the existing mapping before you can re-map the User Profile Field'), 'error');
  }
  else {
    if (is_null($contact_manager) || !drupal_strlen(trim($contact_manager)) || is_null($profile) || !drupal_strlen(trim($profile))) {
      drupal_set_message(t('Field mapping must contain a value in order to be mapped correctly'));
    }
    else {
      $mappings[$contact_manager] = $profile;
      variable_set('contact_manager_field_mappings', $mappings);
      drupal_set_message(t('User Profile Field mapping has been created'));
    }
  }
  $form_state['redirect'] = 'admin/settings/contact_manager';
}

/**
 * Remove a mapping from the contact manager module
 */
function contact_manager_admin_mappings_delete($mapping = NULL) {
  if (is_null($mapping) || !drupal_strlen($mapping)) {
    watchdog('contact_manager', t('Invalid mapping %mapping was received by the module'), array('%mapping' => $mapping), WATCHDOG_ERROR);
    drupal_set_message(t('Invalid mapping %mapping received', array('%mapping' => $mapping), 'error'));
  }
  else {
    $mappings = variable_get('contact_manager_field_mappings', array());
    if (isset($mappings[$mapping])) {
      unset($mappings[$mapping]);
    }
    variable_set('contact_manager_field_mappings', $mappings);
    drupal_set_message(t('User Profile Field mapping has been removed'));
  }
  drupal_goto('admin/settings/contact_manager');
}
