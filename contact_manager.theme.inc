<?php
// $id

/**
 * Theme the contact manager mappings form
 */
function theme_contact_manager_mappings_form($form) {
  $output = '';
  if (isset($form['mappings'])) {
    $output = drupal_render($form['mappings']);
  }
  $rows = array();
  $rows[] = array(
    'contact_manager' => drupal_render($form['contact_manager']),
    'profile' => drupal_render($form['profile']),
    'link' => drupal_render($form['submit']),
  );
  $output .= theme('table', NULL, $rows);
  $output .= drupal_render($form);
  return $output;
}
