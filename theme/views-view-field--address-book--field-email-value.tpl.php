<?php
?>

<?php
  // only doing this registry theme edit if the webmail_plus module is available
  if (module_exists('webmail_plus')) {
    $url = l($row->{$field->field_alias}, 'webmail_plus/compose', array('query' => array('to' => $row->{$field->field_alias})));
  }
  else {
    $url = '<a href="mailto:'. $row->{$field->field_alias} .'">'. $row->{$field->field_alias}. '</a>';
  }
  print $url;
?>
