<?php
// $Id

/**
 * Function to search for contacts in the contact manager
 */
function contact_manager_search_results() {
  global $user;
  $sql = sprintf(_contact_manager_search_sql(), $user->uid);

  // Table headers that we will utilize in the contact manager search view
  $headers = array(t('Contact Name'), t('Organization'), t('Title'), t('Phone #'), t('Email'));

  $rows = array();
  $result = db_query($sql);
  // Step through each of the rows and build the view we are displaying
  while ($row = db_fetch_object($result)) {
    if (is_numeric($row->profile_uid)) {
      $name = theme('username', user_load(array('uid' => $row->profile_uid)));
    }
    else {
      $name = l($row->contact_first_name ." ". $row->contact_last_name, 'node/'. $row->nid);
    }
    $org = (is_null($row->contact_organization)) ? $row->profile_organization : $row->contact_organization;
    $title = (is_null($row->contact_title)) ? $row->profile_title : $row->contact_title;
    $phone = (is_null($row->contact_phone)) ? $row->profile_phone : $row->contact_phone;
    $email = (is_null($row->contact_email)) ? $row->profile_email : $row->contact_email;
    $rows[] = array($name, $org, $title, $phone, $email);
  }
  return theme('table', $headers, $rows);
}

/**
 *
 */
function contact_manager_search_autocomplete($string) {
  global $user;

  // Do not return any data if nothing comes in
  if (!drupal_strlen(trim($string))) {
     return;
  }

  // grab the incoming string, it might already contain something
  $pieces = preg_split("/(,)/", $string);
  $last = $pieces[sizeof($pieces)-1];
  // take out the last record since it's incomplete and we're trying to match it
  unset($pieces[count($pieces) - 1]);
  $base_string = implode(', ', $pieces);
  if (drupal_strlen(trim($base_string))) {
    $base_string .= ', ';
  }

  // Retrieve the sql statement we wish to run and add user information and any string information
  $sql = sprintf(_contact_manager_search_sql(), $user->uid);
  // Building a where clause for autocomplete so we can search for the pertinent records based off the string
  if (drupal_strlen(trim($last))) {
    $sql .= _contact_manager_build_where_clause(trim($last));
  }
  $data = array();
  $result = db_query($sql);
  // Step through each of the rows and build the view we are displaying
  while ($row = db_fetch_object($result)) {
    $rowdata = array();
    if (is_numeric($row->profile_uid)) {
      $rowdata['username'] = theme('username', user_load(array('uid' => $row->profile_uid)));
    }
    else {
      $rowdata['username'] = l($row->contact_first_name ." ". $row->contact_last_name, 'node/'. $row->nid);
    }
    $rowdata['organization'] = (is_null($row->contact_organization)) ? $row->profile_organization : $row->contact_organization;
    $rowdata['title'] = (is_null($row->contact_title)) ? $row->profile_title : $row->contact_title;
    $rowdata['phone'] = (is_null($row->contact_phone)) ? $row->profile_phone : $row->contact_phone;
    $rowdata['email'] = (is_null($row->contact_email)) ? $row->profile_email : $row->contact_email;

    $record = "";
    if ($rowdata['email']) {
      // if the first name and last name exist, great
      if ($row->contact_first_name && $row->contact_last_name) {
        $record = $row->contact_first_name.' '.$row->contact_last_name.' <'.$rowdata['email'].'>';
      }
      else {
        // if they dont just add the email
        $record = $rowdata['email'];
      }
      $data[$base_string.$record] = $record;
    }
  }

  // make sure nothing gets cached
  header("Cache-Control: no-cache, must-revalidate");
  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  print drupal_to_js($data);
  exit();
}

