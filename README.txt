
The contact manager module tries to be a central repository for user contacts in drupal.

INSTALLATION
------------

You need the following modules installed and enabled as they are a requirement for the Contact Manager.

 * Content Profile (http://drupal.org/project/content_profile)
 * CCK (http://drupal.org/project/cck)

You need to enable the following CCK modules at minimum:

 * Content
 * Content Copy
 * Fieldgroup
 * Node Reference
 * Text

Install and enable the Contact Manager Drupal module as you would any Drupal module.

Configure the module at Administer > Site Configuration > Contact Manager

CREDITS
-------
Developed and maintained by Darren Ferguson <http://www.openbandlabs.com/crew/darrenferguson>
Co-Developed and maintained by Denis Voitenko <denis.voitenko [at] gmail [dot] com>
Development sponsored by OpenBand, a subsidiary of M.C.Dean, Inc. <http://www.openbandlabs.com/>
